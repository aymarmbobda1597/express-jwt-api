/**
 * JSON WEB TOKEN REST API
 */

 'use strict';

//  Import Modules
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const dotenv = require('dotenv');
const helmet = require('helmet');
const morgan = require('morgan');
const express = require('express');
//const mail = require('./utils/mail');
const swaggerUi = require('swagger-ui-express');
const headers = require('./middlewares/headers');
const userRoutes = require('./routes/user_route');
const roleRoutes = require('./routes/role_route');
const authRoutes = require('./routes/auth_route');
const constants = require('./configurations/datas/variables');
const connectDB = require('./configurations/database/connexion');
const swaggerDocument = require('./configurations/swagger/openapi.json');

//  Configurations Definitions
dotenv.config();
connectDB.connexion();
//mail.verifyConnection();

//  Variables Definitions
const app = express()
const options = { explorer: true };
const port = constants.port || 8080;
//const port = process.env.NODE_ENV === 'production' ? 8080 : process.env.PORT
const corsOptions = { origin: '*', methods: 'GET, HEAD, PUT, POST, DELETE', preflightContinue: false };
const accessLogStream = fs.createWriteStream(path.join(__dirname, '../../logs/jwt.logs'), { flags: 'a' });

//  Middlewares Definitions
app.use(helmet());
app.use(headers.headers);
app.use(cors(corsOptions));
app.use(express.json({ limit: '50mb' }));
//app.use(morgan('dev'));
app.use(morgan('combined', { stream: accessLogStream }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));

//  Swagger Documentation
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

//  Routes Middlewares
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/roles', roleRoutes);

//  Launch application server
app.listen(port, () => console.log(`Server is 🏃 on http://127.0.0.1:${port}`))