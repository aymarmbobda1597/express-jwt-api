/**
 *    HEADERS MIDDLEWARE
 */

 'use strict';

//  Import Modules

//  Variables Definition

//  Function Definition
exports.headers = (request, response, next) => {
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Methods", "*");
    response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, " + "Access-Control-Request-Method, " + "Access-Control-Request-Headers, " + "Authorization, " + "RefreshAuthorization");
    response.setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin," + "Access-Control-Allow-Credentials, Authorization, RefreshAuthorization");
  
    next();
};