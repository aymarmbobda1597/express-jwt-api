/**
 * AUTHORIZATION MIDDLEWARE
 */

 'use strict';

//  Import Modules
const jwt = require('jsonwebtoken');
const User = require('../models/User_model');
const Role = require('../models/Role_model');

//  Functions Definitions
exports.authorize = (permissions) => (request, response, next) => {
    const BearerToken = request.get('Authorization');
    if (!BearerToken) {
        return response.status(401).json({
            'message': 'Invalid Token'
        });
    }

    const token = BearerToken.split(' ')[1];
    if (!token) {
        return response.status(401).json({
            message: 'Invalid Token'
        })
    }
    
    
    jwt.verify(token, process.env.TOKEN_SECRET, (error, payload) => {
        if (error) {
            return response.status(401).json({
                'message': 'Invalid Token'
            }); 
        }
    
        User
            .findById(payload.id)
            .then(user => {
                if (!user) {
                    const error = new Error('Forbidden');
                    error.status = 403;
                    throw error;
                }

                return Role.findById(user.role.toString());
            })
            .then(role => {
                /*is_authorized = permissions.filter(role => role.name == permissions);
                if (!is_authorized) {
                    const error = new Error('Forbidden');
                    error.code = 403;
                    throw error;
                }*/
                if (permissions) {
                    if (!permissions.filter(role => role.name == permissions)) {
                        const error = new Error('Forbidden');
                        error.code = 403;
                        throw error
                    }
                }
            
                next();
            })
            .catch(error => {
                return response.status(error.status || 403).json({
                    'message': error.message
                }); 
            })
    });
}