/**
 * VALIDATION FUNCTION
 */

 'use strict';

//  Import Modules
const Joi = require('@hapi/joi');

//  Variables Definitions

//  Functions Definitions
exports.roleValidation = (request, response, next) => {
    const schema = Joi.object({
        name: Joi.string().min(2).max(25).required(),
        description: Joi.string().min(6).required()
    });

    schema
        .validateAsync(request.body)
        .then(result => {
            next();
        })
        .catch(error => {
            return response.status(400).json({
                message: error.details[0].message
            })
        });
};

exports.signUpValidation = (request, response, next) => {
    const schema = Joi.object({
        lastname: Joi.string().min(2).max(50).required(),
        firstname: Joi.string().min(2).max(50).required(),
        email: Joi.string().min(6).required().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'org'] } }),
        password: Joi.string().min(6).pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
        repeat_password: Joi.ref('password')
    });

    schema
        .validateAsync(request.body)
        .then(result => {
            next();
        })
        .catch(error => {
            return response.status(400).json({
                message: error.details[0].message
            })
        });
};