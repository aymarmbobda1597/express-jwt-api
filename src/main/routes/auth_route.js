/**
 * AUTHENTIFICATION ROUTE
 */

 'use strict';

//  Import Modules
const express = require('express');
const validation = require('../middlewares/validation');
const authorization = require('../middlewares/authorization');
const userController = require('../controllers/user_controller');

//  Variables Definitions
const router = express();

//  Routes Definitions
router.post('/signIn', userController.signIn);

router.post('/signUp', [validation.signUpValidation], userController.signUp);

router.post('/forgot-password', userController.forgotPassword);

router.post('/refresh-token', [authorization.authorize()], userController.refreshToken);

//  Export Modules
module.exports = router;