/**
 * USER ROUTES
 */

 'use strict';

//  Import Modules
const express = require('express');
const validation = require('../middlewares/validation');
const authorization = require('../middlewares/authorization');
const roleController = require('../controllers/role_controller');

//  Variables Defintions
const router = express.Router();

//  Routes Definitions
router.get('', [authorization.authorize(['ADMIN', 'USER'])], roleController.findAll);

router.get('/:id', [authorization.authorize(['ADMIN', 'USER'])], roleController.findById)

router.post('', [authorization.authorize(['ADMIN', 'SUPERADMIN']), validation.roleValidation], roleController.create);

router.put('/:id', [authorization.authorize(['ADMIN', 'SUPERADMIN']), validation.roleValidation], roleController.update);

//  router.delete('/:id', [authorization.authorize(['HIPERADMIN'])], roleController.delete);

//  Export Modules
module.exports = router;