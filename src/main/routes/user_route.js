/**
 * USER ROUTES
 */

 'use strict';

//  Import Modules
const express = require('express');
const mail = require('../utils/mail');
const files = require('../utils/files');
const userController = require('../controllers/user_controller');

//  Variables Defintions
const router = express.Router();

//  Routes Definitions
router.get('', userController.findAll);

router.get('/:id', userController.findById)

router.post('', userController.create);

router.put('/:id', userController.update);

router.delete('/:id', userController.delete);

router.post('/mail', (request, response) => {
    const datas = {
        to: 'address1@gmail.com, adrress2@yahoo.fr',
        subject: 'Hello my friends✔',
        text: 'Hello world?',
        attachments: [{ filename: 'jwt.logs', path: files.clearPath('../../logs/jwt.logs') }]
    }
    
    mail.sendMailWithAttachments(datas)
        .then(info => {
            if (!info) {
                const error = new Error('Error');
                error.status = 400;
                throw error;
            }
            
            const { messageId } = info;
            return response.status(200).json({
                message: 'Email Sended Successfull',
                message_id: messageId
            })
        })
        .catch(error => {
            return response.status(error.status || 500).json({
                message: error.message
            })
        })
});

//  Export Modules
module.exports = router;