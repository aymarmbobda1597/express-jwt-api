/**
 * USER CONTROLLER
 */

 'use strict';

//  Import Modules
const bcrypt = require('bcrypt');
const files = require('../utils/files');
const Role = require('../models/Role_model');
const User = require('../models/User_model');
const security = require('../utils/security');
const HttpStatus = require('../configurations/datas/HttpStatus');

//  Variables Definitions
const EXPIRES_IN_MINUTES = '1440m';

//  Functions Definitions
exports.signUp = (request, response) => {
    let intermediate_datas = null;

    Role
        .findOne({ name: 'USER'})
        .then(role => {
            if (!role) {
                const error = new Error('Role Not Found');
                error.code = HttpStatus.NOT_FOUND;
                throw error;
            }

            intermediate_datas = role;
            return User.findOne({ email: request.body.email });
        })
        .then(userFinded => {
            if (userFinded) {
                const error = new Error('A user with this email already exist');
                error.code = HttpStatus.CONFLICT;
                throw error;
            }
        
            const { firstname, lastname, email, password } = request.body;
            const user = new User({
                role: intermediate_datas,
                email: email,
                lastname: lastname,
                firstname: firstname,
                password: security.generateHashedPassword(password)
            });

            return user.save();
        })
        .then(user => {
            role = intermediate_datas;
            role.users.push(user);

            intermediate_datas = user;
            return role.save();
        })
        .then(result => {
            const { password, ...other } = intermediate_datas._doc;
            const { access_token, refresh_token } = security.generateToken(other, EXPIRES_IN_MINUTES);

            return response.status(HttpStatus.CREATED).json({
                message: 'User Register Successfully',
                token: {
                    token_type: 'Bearer',
                    access_token: access_token,
                    refresh_token: refresh_token
                },
                user: {
                    id: other._id.toString(),
                    email: other.email,
                    createdAt: other.createdAt.toISOString(),
                    updatedAt: other.updatedAt.toISOString() 
                }
            })
        })
        .catch(error => {
            console.log(error);
            return response.status(error.code || HttpStatus.SERVER_ERROR).json({
                message: error.message
            })
        });
};

exports.signIn = (request, response) => {
    let intermediate_datas = null;
    const { email, password } = request.body;

    User
        .findOne({ email: email })
        .then(user => {
            if (!user) {
                const error = new Error('Invalid email or password');
                error.status = HttpStatus.BAD_REQUEST;
                throw error;
            }

            intermediate_datas = user;
            return bcrypt.compare(password, user.password);
        })
        .then(equal => {
            if (!equal) {
                const error = new Error('Invalid email or password');
                error.status = HttpStatus.BAD_REQUEST;
                throw error;
            }

            const { password, ...other } = intermediate_datas._doc;
            const { access_token, refresh_token } = security.generateToken(other, EXPIRES_IN_MINUTES);

            return response.status(HttpStatus.OK).json({
                message: 'User SignIn Successfull',
                token: {
                    token_type: 'Bearer',
                    access_token: access_token,
                    refresh_token: refresh_token
                },
                user: {
                    id: other._id.toString(),
                    email: other.email,
                    createdAt: other.createdAt.toISOString(),
                    updatedAt: other.updatedAt.toISOString() 
                }
            });
        })
        .catch(error => {
            return response.status(error.status || HttpStatus.SERVER_ERROR).json({
                message: error.message
            })
        });
}

exports.forgotPassword = (request, reponse) => {
    return response.status(200).json({
        message: 'Forgot Passsword'
    });
};

exports.refreshToken = (request, response) => { 
    const { token } = request.body;
    const refreshTokens = files.exportJsonDatas('../configurations/datas/refresh.json');

    if (!token) {
        return response.status(HttpStatus.UNAUTHORIZED).json({
            message: 'Invalid Token'
        });
    }

    if (!refreshTokens.filter(element => element.refresh_token == token)) {
        return response.status(HttpStatus.FORBIDDEN).json({
            message: 'Forbidden'
        });
    }

    try {
        const new_token = security.refreshToken(token, EXPIRATION_TIME);
        
        return response.status(HttpStatus.OK).json({
            message: 'Token Refreshed Successfull',
            token: {
                token_type: 'Bearer',
                access_token: new_token.access_token
            },
        });
    }
    catch(error) {
        return response.status(error.status || HttpStatus.SERVER_ERROR).json({
            message: 'Invalid Token'
        });
    }
};

exports.findAll = (request, response) => {
    let total = 0;
    const page = (!isNaN(request.query.page)) ? parseInt(request.query.page) : 1;
    const per_page = (!isNaN(request.query.per_page)) ? parseInt(request.query.per_page) : 25;
    const keyword = request.query.keyword || '';

    User.find({ 
            lastname: { $regex: keyword, $options: 'xi' }, 
            firstname: { $regex: keyword, $options: 'xi' }, 
            email: { $regex: keyword, $options: 'xi' } 
        })
        .countDocuments()
        .then(count => {
            total = count;
            
            return User.find({ 
                            lastname: { $regex: keyword, $options: 'xi' }, 
                            firstname: { $regex: keyword, $options: 'xi' }, 
                            email: { $regex: keyword, $options: 'xi' } 
                        })
                    .skip((page - 1) * per_page).sort({ createdAt: -1 }).limit(per_page);
        })
        .then(users => {
            users = users.filter(user => user.status != 'DELETED');

            return response.status(HttpStatus.OK).json({
                total: total,
                pages: Math.ceil(total / per_page),
                per_page: per_page,
                current_page: page,
                message: 'Users Fetched Successfully!',
                users: users.map(user => {
                    const { password, ...other } = user._doc;

                    return {
                        id: other._id.toString(),
                        email: other.email,
                        createdAt: other.createdAt.toISOString(),
                        updatedAt: other.updatedAt.toISOString() 
                    }
                })
            });
        })
        .catch(error => {
            return response.status(500).json({
                message: error.message
            });
        });
};

exports.findById = (request, response) => {
    const user_id = request.params.id
    
    User
        .findById(user_id)
        .then(user => {
            if (!user) {
                const error = new Error('User Not Found');
                error.status = HttpStatus.NOT_FOUND;
                throw error;
            }

            const { password, ...other } = user._doc;

            return response.status(HttpStatus.OK).json({
                message: 'User Finded Successfully!',
                role: {
                    id: other._id.toString(),
                    email: other.email,
                    description: description,
                    createdAt: other.createdAt.toISOString(),
                    updatedAt: other.updatedAt.toISOString() 
                }
            });
        })
        .catch(error => {
            return response.status(error.status || HttpStatus.SERVER_ERROR).json({
                message: error.message
            });
        });
};

exports.create = (request, response) => {
    response.status(HttpStatus.OK).json({
        message: 'OK'
    });
};

exports.update = (request, response) => {
    response.status(HttpStatus.OK).json({
        message: 'OK'
    });
};

exports.delete = (request, response) => {
    const user_id = request.params.id;

    User
        .findById(user_id)
        .then(user => {
            if (!user) {
                const error = new Error('User Not Found');
                error.status = HttpStatus.NOT_FOUND;
                throw error;
            }

            user.status = 'DELETED';
            return user.save();
        })
        .then(result => {
            return response.status(HttpStatus.OK).json({
                message: 'User Deleted Successfull'
            });
        })
        .catch(error => {
            console.log(error);
            return response.status(error.code || HttpStatus.BAD_REQUEST).json({
                message: error.message
            })
        })
};