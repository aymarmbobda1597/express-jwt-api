/**
 * USER CONTROLLER
 */

 'use strict';

//  Import Modules
const Role = require('../models/Role_model');
const validation = require('../utils/validation');

//  Variables Definitions

//  Functions Definitions
exports.findAll = (request, response) => {
    let total = 0;
    const page = request.query.page || 1;
    const per_page = request.query.per_page || 25;
    const keyword = request.query.keyword || '';

    Role
        .find({ name: { $regex: keyword, $options: 'xi' } })
        .countDocuments()
        .then(count => {
            total = count;
            return Role.find({ name: { $regex: keyword, $options: 'xi' } }).skip((page - 1) * per_page).sort({ createdAt: -1 }).limit(per_page);
        })
        .then(roles => {
            return response.status(200).json({
                total: total,
                pages: Math.ceil(total / per_page),
                per_page: per_page,
                current_page: page,
                message: 'Roles Fetched Successfully!',
                roles: roles.map(role => {
                    const { name, description } = role._doc;

                    return {
                        id: role._id.toString(),
                        name: name,
                        description: description,
                        createdAt: role.createdAt.toISOString(),
                        updatedAt: role.updatedAt.toISOString()
                    };
                })
            });
        })
        .catch(error => {
            return response.status(500).json({
                message: error.message
            });
        });
};

exports.findById = (request, response) => {
    const role_id = request.params.id;

    Role
        .findById(role_id)
        .then(role => {
            if (!role) {
                const error = new Error('Role Not Found');
                error.status = 404;
                throw error;
            }

            const { name, description } = role._doc;
            return response.status(200).json({
                message: 'Role Finded Successfully!',
                role: {
                    id: role._id.toString(),
                    name: name,
                    description: description,
                    createdAt: role.createdAt.toISOString(),
                    updatedAt: role.updatedAt.toISOString() 
                }
            });
        })
        .catch(error => {
            return response.status(error.status || 500).json({
                message: error.message
            });
        });
};

exports.create = (request, response) => {
    const { name, description } = request.body;

    const role = new Role({
        name: name,
        description: description
    });

    role
        .save()
        .then(role => {
            if (!role) {
                const error = new Error('Bad Request');
                error.status = 400;
                throw error;
            }

            const { name, description } = role._doc;
            return response.status(201).json({
                message: 'Role Created Successfully!',
                role: {
                    id: role._id.toString(),
                    name: name,
                    description: description,
                    createdAt: role.createdAt.toISOString(),
                    updatedAt: role.updatedAt.toISOString() 
                }
            });
        })
        .catch(error => {
            return response.status(error.status || 500).json({
                message: error.message
            });
        });
};

exports.update = (request, response) => {
    const role_id = request.params.id;

    Role
        .findById(role_id)
        .then(role => {
            if (!role) {
                const error = new Error('Role Not Found');
                error.status = 404;
                throw error;
            }

            const { name, description } = request.body;

            role.name = name || role.name;
            role.description = description || role.description;
            
            return role.save();
        })
        .then(role => {
            const { name, description } = role._doc;
            return response.status(200).json({
                message: 'Role Updated Successfull',
                role: {
                    id: role._id.toString(),
                    name: name,
                    description: description,
                    createdAt: role.createdAt.toISOString(),
                    updatedAt: role.updatedAt.toISOString()
                }
            });
        })
        .catch(error => {
            return response.status(error.status || 500).json({
                message: error.message
            })
        });
};

exports.delete = (request, response) => {
    const role_id = request.params.id;

    Role
        .findById(role_id)
        .then(role => {
            if (!role) {
                const error = new Error('Role Not Found');
                error.status = 404;
                throw error;
            }

            return Role.findByIdAndRemove(role_id);
        })
        .then(result => {
            return response.status(200).json({
                message: 'Role Deleted Successfull  '
            })
        })
        .catch(error => {
            return response.status(error.status || 500).json({
                message: error.message
            });
        });
};