/**
 * JSON WEB TOKEN REST API
 */

 'use strict';

//  Import Modules

//  Variables Definitions

//  Export Modules
module.exports = {
    OK: 200,
    CREATED: 201,
    ACCEPTED: 202,
    NOCONTENT: 204,
    FOUND: 302,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    PAYMENT_REQUIRED: 402,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    NOT_ACCEPTABLE: 406,
    CONFLICT: 409,
    SERVER_ERROR: 500,
}