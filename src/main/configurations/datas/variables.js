/**
 * JSON WEB TOKEN REST API
 */

 'use strict';

//  Import Modules
const dotenv = require('dotenv');

//  Configurations Definitions
dotenv.config();

//  Export Modules
module.exports = {
    port: process.env.PORT,
    mongo_uri: process.env.MONGO_URI,
    token_secret: process.env.TOKEN_SECRET,
    mail_username: process.env.MAIL_USERNAME,
    mail_password: process.env.MAIL_PASSWORD,
    refresh_token_secret: process.env.REFRESH_TOKEN_SECRET,
}