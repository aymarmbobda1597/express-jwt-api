/**
 *    DATABASE CONNEXION
 **/

'use strict';

//  Imports Modules
const mongoose = require('mongoose');
const constants = require('../datas/variables');

//  Variables Definition

//  Function Definition
exports.connexion = async () => {
    try {
        const connexion = await mongoose.connect(constants.mongo_uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log(`Successfully Connect To MongoDB Cluster`);
    } catch (error) {
        console.log(error.message);
        process.exit(1);
    }
};