/**
 *  MAIL FUNCTIONS
 */

 'use strict';

//  Import Modules
const nodemailer = require('nodemailer');
const constants = require('../configurations/datas/variables');

//  Variables Definitions
const transport = {
    debug: true,
    logger: true,
    service: 'gmail',
    auth: {
        user: constants.mail_username,
        pass: constants.mail_password
    }
};
const transporter = nodemailer.createTransport(transport);

//  Functions Definitions
exports.verifyConnection = () => {
    transporter.verify((error, success) => {
        if(error) {
          //if error happened code ends here
          console.error(error)
        } else {
          //this means success
          console.log('users ready to mail myself')
        }
    });
}

exports.sendMailWithAttachments = async (datas) => {
    const { to, subject, text, attachments } = datas;
    const mailData = {
        from: constants.mail_username,
        to: to,
        subject: subject,
        text: text,
        html: '<b>Hey there!</b>',
        attachments: attachments
    };

    return await transporter.sendMail(mailData);
}