/**
 * JSON WEB TOKEN FUNCTIONS
 */

 'use strict';

//  Import Modules
const bcrypt = require('bcrypt')
const {sign, verify} = require('jsonwebtoken');
const constants = require('../configurations/datas/variables');

//  Variables Definitions
const saltRounds = 10;

//  Functions Definitions
exports.generateHashedPassword = (password) => {
    const salt = bcrypt.genSaltSync(saltRounds);

    return bcrypt.hashSync(password, salt);
}

exports.isIdentiquePassword = (hashedPassword, password) => {
    bcrypt
        .compare(hashedPassword, password)
        .then(result => {
            return result;
        })
        .cath(error => {
            throw error;
        });
}

exports.generateToken = (data, expiresIn) => {
    const { _id } = data;

    if (!expiresIn) {
        return {
            access_token: sign({ id: _id.toString() }, constants.token_secret)
        }
    }

    return {
        access_token: sign({ id: _id.toString() }, constants.token_secret, { expiresIn: expiresIn }),
        refresh_token: sign({ id: _id.toString() }, constants.refresh_token_secret)
    }
}

exports.refreshToken = (refreshToken, expiresIn) => {
    verify(refreshToken, constants.refresh_token_secret, (error, payload) => {
        if (error) {
            const error = new Error('Invalid Token');
            error.status = 403;
            throw error;
        }

        return sign({ id: payload.id }, constants.token_secret, { expiresIn: expiresIn });
    });
}