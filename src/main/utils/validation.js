/**
 * VALIDATION FUNCTION
 */

 'use strict';

//  Import Modules

//  Variables Definitions
const REGEX_DATE = ` 	
(?:\s|^)(?P<day>(?P<threeone>31)|(?P<thirty>30)|(?P<twonine>29)|(?P<twoeight>2[0-8]|1[0-9]|0?[1-9]))(?P<sep>[/\-\.:])(?P<month>(?(threeone)(?:jan|mar|may|jul|aug|oct|dec))(?(thirty)(?:jan|mar|apr|may|jun|jul|aug|sep|oct|nov|dec))(?(twonine)(?:jan|(?P<feb>feb)|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|))(?(twoeight)`

//  Functions Definitions
exports.validateEmail = (email) => {
    const REGEX_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    
    if (REGEX_EMAIL.test(email)) {
        return true;
    }
    return false;
}

exports.validatePhoneNumber = (phonenumber) => {
    const REGEX_PHONE = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;

    if (phonenumber.match(REGEX_PHONE)) {
        return true;
    }
    return false;
}

exports.saveRoleValidation = async (data) => {
    const schema = Joi.object({
        name: Joi.string().min(2).max(25).required(),
        description: Joi.string().min(6).required()
    });
    
    return await schema.validateAsync(data);
}