/**
 * JSON WEB TOKEN FUNCTIONS
 */

 'use strict';

//  Import Modules
const path = require('path');

//  Variables Definitions

//  Functions Definitions
exports.clearPath = (filePath) => {
    return path.join(__dirname, '..', filePath);
}

exports.exportJsonDatas = (json_file_path) => {
    return require(json_file_path);
}