/**
 * USER MODEL
 */

 'use strict';

//  Import Modules
const mongoose = require('mongoose');

//  Variables Definitions
const Schema = mongoose.Schema;

//  Model Definition
const userSchema = new Schema({
    firstname: {
        min: 2,
        max: 50,
        trim: true,
        type: String,
        required: true
    },
    lastname: {
        min: 2,
        max: 50,
        trim: true,
        type: String,
        required: true
    },
    email: {
        min: 6,
        max: 200,
        trim: true,
        unique: true,
        type: String,
        required: true
    },
    password: {
        min: 6,
        max: 1024,
        type: String,
        required: true
    },
    status: {
        trim: true,
        type: String,
        required: false,
        default: 'ACTIVED',
        enum: ['ACTIVED', 'UNACTIVED', 'DELETED']
    },
    avatar: {
        ref: 'Documents',
        type: Schema.Types.ObjectId
    },
    role: {
        ref: 'Roles',
        required: true,
        type: Schema.Types.ObjectId
    }
}, { timestamps: true });

//  Export Modules
module.exports = mongoose.model('Users', userSchema);