/**
 * DOCUMENT MODEL
 */

 'use strict';

//  Import Modules
const mongoose = require('mongoose');

//  Variables Definitions
const Schema = mongoose.Schema;

//  Model Definition
const documentSchema = new Schema({
    filename: {
        min: 2,
        max: 25,
        type: String,
        required: true,
        uppercase: true
    },
    url: {
        min: 2,
        type: String,
        required: true,
        uppercase: true
    },
    mimetype: {
        min: 2,
        max: 255,
        type: String,
        required: true
    },
    width: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    size: {
        type: Number,
        required: true
    },
    thumbnail: {
        type: JSON,
        required: true
    },
    medium: {
        type: JSON,
        required: true
    },
    users: {
        ref: 'Users',
        type: Schema.Types.ObjectId
    }
}, { timestamps: true });

//  Export Modules
module.exports = mongoose.model('Documents', documentSchema);