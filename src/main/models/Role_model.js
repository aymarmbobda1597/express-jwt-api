/**
 * ROLE MODEL
 */

 'use strict';

//  Import Modules
const mongoose = require('mongoose');

//  Variables Definitions
const Schema = mongoose.Schema;

//  Model Definition
const roleSchema = new Schema({
    name: {
        min: 2,
        max: 25,
        type: String,
        required: true,
        uppercase: true
    },
    description: {
        min: 6,
        max: 255,
        type: String,
        required: true
    },
    users: [
        {
            ref: 'Users',
            type: Schema.Types.ObjectId
        }
    ]
}, { timestamps: true });

//  Export Modules
module.exports = mongoose.model('Roles', roleSchema);